package com.example.uapv1901166.tp1;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        ListView listview = findViewById(R.id.listPays);
//        ArrayAdapter<String> listeNomPays = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, CountryList.getNameArray());
//        listview.setAdapter(listeNomPays);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.listPays);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(new IconicAdapter());
//        listview.setOnItemClickListener(new OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String item = (String) parent.getItemAtPosition(position);
//                Intent intent = new Intent(MainActivity.this, affichePays.class);
//                intent.putExtra("Pays", item);
//                startActivity(intent);
//            }
//        });
//        listview.setOnClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String item = (String) parent.getItemAtPosition(position);
//                Intent intent = new Intent(MainActivity.this, affichePays.class);
//                intent.putExtra("Pays", item);
//                startActivity(intent);
//            }
//        });
    }

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {

        @NonNull
        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.row, parent,false)));
        }

        @Override
        public void onBindViewHolder( final RowHolder holder, int position) {
            holder.bindModel(CountryList.getNameArray()[position]);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String item = (String) holder.label.getText();
                    Intent intent = new Intent(MainActivity.this, affichePays.class);
                    intent.putExtra("Pays", item);
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return CountryList.getNameArray().length;
        }
    }

    static class RowHolder extends RecyclerView.ViewHolder {
        TextView label=null;
        ImageView icon=null;

        public RowHolder(View row) {
            super(row);
            label=(TextView) row.findViewById(R.id.label);
            icon=(ImageView) row.findViewById(R.id.icon);
        }

        void bindModel(String item) {
            label.setText(item);
            icon.setImageResource(CountryList.getCountry(item).getRessource());
        }
    }
}
