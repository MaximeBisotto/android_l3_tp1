package com.example.uapv1901166.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class affichePays extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche_pays);
        Intent intent = getIntent();
        String pays = intent.getStringExtra("Pays");
        final Country country = CountryList.getCountry(pays);

        TextView text_pays = findViewById(R.id.text_pays);
        text_pays.setText(pays);

        final TextView text_capitale = findViewById(R.id.text_capitale);
        text_capitale.setText(country.getmCapital());

        final TextView text_langue = findViewById(R.id.text_langue);
        text_langue.setText(country.getmLanguage());

        final TextView text_monnaie = findViewById(R.id.text_monnaie);
        text_monnaie.setText(country.getmCurrency());

        final TextView text_population = findViewById(R.id.text_population);
        text_population.setText(String.valueOf(country.getmPopulation()));

        final TextView text_superficie = findViewById(R.id.text_superficie);
        text_superficie.setText(String.valueOf(country.getmArea()));

        ImageView image = findViewById(R.id.image_pays);
        int flag = country.getRessource();
        image.setImageResource(flag);

        Button button = (Button) findViewById(R.id.button_sauvegarde);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country.setmCapital(text_capitale.getText().toString());
                country.setmLanguage(text_langue.getText().toString());
                country.setmCurrency(text_monnaie.getText().toString());
                country.setmPopulation(Integer.parseInt(text_population.getText().toString()));
                country.setmArea(Integer.parseInt(text_superficie.getText().toString()));
                Toast.makeText(affichePays.this, "Sauvegarder", Toast.LENGTH_LONG);
            }
        });
    }
}
